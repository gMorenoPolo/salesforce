public class H_class_AutoService implements schedulable {
    String body = '';

    public H_class_AutoService(String body){
        this.body = body;
    }
	public void execute(SchedulableContext ctx) {
        H_class_communicationService.sendEvent(this.body);
    }
}