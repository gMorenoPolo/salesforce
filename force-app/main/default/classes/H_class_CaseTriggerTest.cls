@isTest(SeeAllData=true)
private class H_class_CaseTriggerTest {
    
    @IsTest
    static void testCaseGroups() {
        Test.startTest();
        
        RecordType rt = [Select Id from RecordType where DeveloperName='H_rt_centroProduccion'];
        System.debug('rtID'+rt.Id);
        
        // Create common test account
        Account a = new Account(Name = 'TestAcct', H_fld_surname__c = 'Prueba');
        insert a; 
        
        // CreateCases
        Case c = new Case(Origin = 'Correo electrónico', AccountId = a.Id, Priority = 'Medium', Status = 'New', Subject = 'A10', Description = 'A10', RecordTypeId = rt.Id);
        insert c;
        
        Case c2 = new Case( Origin = 'Correo electrónico', AccountId = a.Id, Priority = 'Medium', Status = 'New', Subject = 'A10, E20', Description = 'A10, E20', RecordTypeId = rt.Id);
        insert c2;
        
        // UpdateCases
        List<Group> grp = [SELECT Id, Type, Name From Group Where DeveloperName= 'H_que_grupoSaco'];
        c.OwnerId = grp[0].Id;
        c2.OwnerId = grp[0].Id;
        update c;
        update c2;
        Test.stopTest();
        //System.assertEquals(false, resultList[0].error);
    }
    
}