global class H_class_ClaimCommunicationService extends H_class_communicationService{
    
    @InvocableMethod(label='Fire api claim event' description='Fire claim event to Marketing Cloud' category='Claim')
    global static void sendEventToMkt(List<Request> pbParamsList){
        Request pbParams = pbParamsList[0];
        //Recogemos los valores de la cabecera del evento:
        String ContactKey = findContactKey(pbParams.AccountId);
        String EventDefinitionKey = pbParams.EventDefinitionKey;
        // Realizamos el mapping de los datos que llegan del siniestro a un objeto pasarela:
        EventWrapper data = mappingWrappedFields(ContactKey,pbParams);
        //Generamos el JSON que irá en el body de la solicitud http POST:
        String body = generateBody(ContactKey,EventDefinitionKey,data);
        //Encolamos la llamada asíncrona:
        sendEvent(body);
    }
    
    //Datos de entrada del PB para Claim  
    global class Request {
        @InvocableVariable(required=true)
        global String AccountId;
        @InvocableVariable(required=true)
        global String EventDefinitionKey;
        @InvocableVariable(required=false)
        global String Email;
        @InvocableVariable(required=false)
        global String MobilePhone;
        @InvocableVariable(required=true)
        global String Name;
        @InvocableVariable(required=false)
        global String PolicyNumberId;
        @InvocableVariable(required=false)
        global String ProductoPoliza;
        @InvocableVariable(required=false)
        global String PeritoAsignado;
        @InvocableVariable(required=false)
        global String ClaveSiniestro;
        @InvocableVariable(required=false)
        global String Nombre;
        @InvocableVariable(required=false)
        global String ClaimReasonType;
        @InvocableVariable(required=false)
        global String AfectaBonusMalus;
        @InvocableVariable(required=false)
        global String Apellido;
        @InvocableVariable(required=false)
        global String PersonalesMateriales;
        @InvocableVariable(required=false)
        global String IncidentSiteCity;
        @InvocableVariable(required=false)
        global String IncidentSiteState;
        @InvocableVariable(required=false)
        global Date InitiationDate;
        @InvocableVariable(required=false)
        global Date FinalizedDate;
        @InvocableVariable(required=false)
        global Date AssessmentDate;
        @InvocableVariable(required=false)
        global Decimal PtePago;
        @InvocableVariable(required=false)
        global Decimal PteRecobro;
        @InvocableVariable(required=false)
        global Decimal TotalPagado;
        @InvocableVariable(required=false)
        global Decimal TotalRecobrado;
        @InvocableVariable(required=false)
        global String Descripcion1;
        @InvocableVariable(required=false)
        global String NombreMediador;
        @InvocableVariable(required=false)
        global String ApellidoMediador;
        @InvocableVariable(required=false)
        global String TelefonoMediador;
        @InvocableVariable(required=false)
        global String ReferenASITURSersanet;
        @InvocableVariable(required=false)
        global String SiniestroNAE;
        @InvocableVariable(required=false)
        global String ClaimReason;
        @InvocableVariable(required=false)
        global String CategoriaAE;
        @InvocableVariable(required=false)
        global String Rehuses;
        @InvocableVariable(required=false)
        global String TipoDeLetrado;
        @InvocableVariable(required=false)
        global String RamoDelProducto;
        @InvocableVariable(required=false)
        global String ControlDeRecibo;
        @InvocableVariable(required=false)
        global String ReciboPendiente;
        @InvocableVariable(required=false)
        global String ReciboCobrado;
        @InvocableVariable(required=false)
        global String PersonaFisicaJuridica;
        @InvocableVariable(required=false)
        global String Severity;
        @InvocableVariable(required=false)
        global Integer IVA;
        @InvocableVariable(required=false)
        global Double ActualAmount;
        @InvocableVariable(required=false)
        global Double EstimatedAmount;
        @InvocableVariable(required=false)
        global String IDEncargo;
        @InvocableVariable(required=false)
        global String CategoriaObjetivo;
        @InvocableVariable(required=false)
        global String CodigoObjetivo;
        @InvocableVariable(required=false)
        global String ContadorAvances;
        @InvocableVariable(required=false)
        global String DiasObjetivo;
        @InvocableVariable(required=false)
        global String EstadoFlujoControl;
        @InvocableVariable(required=false)
        global Date FechaEstado;
        @InvocableVariable(required=false)
        global Date FechaEstadoControl;
        @InvocableVariable(required=false)
        global Date FechaInicioObjetivo;
        @InvocableVariable(required=false)
        global String Siniestro;
        @InvocableVariable(required=false)
        global Date Fechaultimocontrol;
        @InvocableVariable(required=false)
        global String TipoEncargo;
        @InvocableVariable(required=false)
        global String Phone;
        @InvocableVariable(required=false)
        global String EstadoAccion;
        @InvocableVariable(required=false)
        global Date FechaAlta;
        @InvocableVariable(required=false)
        global String HoraAccion;
        @InvocableVariable(required=false)
        global Integer NumEncargo;
        @InvocableVariable(required=false)
        global String Comunicacion;
        
        // Params of the additional data Claim from PB
        @InvocableVariable(required=false)
        global String UsuarioAlta;
        @InvocableVariable(required=false)
        global String Complejidad;
        @InvocableVariable(required=false)
        global String CanalAlta;
    }
    
    global class EventWrapper {
        global String Contactkey;
        global String Name;
        global String Email;
        global String MobilePhone;
        global String PolicyNumberId;
        global String ProductoPoliza;
        global String PeritoAsignado;
        global String AccountId;
        global String ClaveSiniestro;
        global String Nombre;
        global String ClaimReasonType;
        global String AfectaBonusMalus;
        global String Apellido;
        global String PersonalesMateriales;
        global String IncidentSiteCity;
        global String IncidentSiteState;
        global Date InitiationDate;
        global Date FinalizedDate;
        global Date AssessmentDate;
        global Decimal PtePago;
        global Decimal PteRecobro;
        global Decimal TotalPagado;
        global Decimal TotalRecobrado;
        global String Descripcion1;
        global String NombreMediador;
        global String ApellidoMediador;
        global String TelefonoMediador;
        global String ReferenASITURSersanet;
        global String SiniestroNAE;
        global String ClaimReason;
        global String CategoriaAE;
        global String Rehuses;
        global String TipoDeLetrado;
        global String RamoDelProducto;
        global String ControlDeRecibo;
        global String UsuarioAlta;
        global String Complejidad;
        global String CanalAlta;
        global String ReciboPendiente;
        global String ReciboCobrado;
        global String PersonaFisicaJuridica;
        global String Severity;
        global Integer IVA;
        global Double ActualAmount;
        global Double EstimatedAmount;
        global String IDEncargo;
        global String CategoriaObjetivo;
        global String CodigoObjetivo;
        global String ContadorAvances;
        global String DiasObjetivo;
        global String EstadoFlujoControl;
        global Date FechaEstado;
        global Date FechaEstadoControl;
        global Date FechaInicioObjetivo;
        global String Siniestro;
        global Date Fechaultimocontrol;
        global String TipoEncargo;
        global String Phone;
        global String EstadoAccion;
        global Date FechaAlta;
        global String HoraAccion;
        global Integer NumEncargo;
        global String Comunicacion;
    }
    
    //Realiza un mapping al objeto Data que viaja en el evento
    public static EventWrapper mappingWrappedFields(String contactKey, Request pbParams){
        EventWrapper data = new EventWrapper();
        data.Contactkey = contactKey;
        data.Name = pbParams.Name;
        data.Email = pbParams.Email;
        data.MobilePhone = pbParams.MobilePhone;
        data.PolicyNumberId = pbParams.PolicyNumberId;
        data.ProductoPoliza = pbParams.ProductoPoliza;
        data.PeritoAsignado = pbParams.PeritoAsignado;
        data.ClaveSiniestro = pbParams.ClaveSiniestro;
        data.Nombre = pbParams.Nombre;
        data.ClaimReasonType = pbParams.ClaimReasonType;
        data.AfectaBonusMalus = pbParams.AfectaBonusMalus;
        data.Apellido = pbParams.Apellido;
        data.PersonalesMateriales = pbParams.PersonalesMateriales;
        data.IncidentSiteCity = pbParams.IncidentSiteCity;
        data.IncidentSiteState = pbParams.IncidentSiteState;
        data.InitiationDate = pbParams.InitiationDate;
        data.FinalizedDate = pbParams.FinalizedDate;
        data.AssessmentDate = pbParams.AssessmentDate;
        data.PtePago = pbParams.PtePago;
        data.PteRecobro = pbParams.PteRecobro;
        data.TotalPagado = pbParams.TotalPagado;
        data.TotalRecobrado = pbParams.TotalRecobrado;
        data.Descripcion1 = pbParams.Descripcion1;
        data.NombreMediador = pbParams.NombreMediador;
        data.ApellidoMediador = pbParams.ApellidoMediador;
        data.TelefonoMediador = pbParams.TelefonoMediador;
        data.ReferenASITURSersanet = pbParams.ReferenASITURSersanet;
        data.SiniestroNAE = pbParams.SiniestroNAE;
        data.ClaimReason = pbParams.ClaimReason;
        data.CategoriaAE = pbParams.CategoriaAE;
        data.Rehuses = pbParams.Rehuses;
        data.TipoDeLetrado = pbParams.TipoDeLetrado;
        data.RamoDelProducto = pbParams.RamoDelProducto;
        data.UsuarioAlta = pbParams.UsuarioAlta;
        data.ControlDeRecibo = pbParams.ControlDeRecibo;
        data.Complejidad = pbParams.Complejidad;
        data.CanalAlta = pbParams.CanalAlta;
        data.ReciboPendiente = pbParams.ReciboPendiente;
        data.ReciboCobrado = pbParams.ReciboCobrado;
        data.PersonaFisicaJuridica = pbParams.PersonaFisicaJuridica;
        data.Severity = pbParams.Severity;
        data.IVA = pbParams.IVA;
        data.ActualAmount = pbParams.ActualAmount;
        data.EstimatedAmount = pbParams.EstimatedAmount;
        data.IDEncargo= pbParams.IDEncargo;
        data.CategoriaObjetivo= pbParams.CategoriaObjetivo;
        data.CodigoObjetivo= pbParams.CodigoObjetivo;
        data.ContadorAvances= pbParams.ContadorAvances;
        data.DiasObjetivo= pbParams.DiasObjetivo;
        data.EstadoFlujoControl= pbParams.EstadoFlujoControl;
        data.FechaEstado= pbParams.FechaEstado;
        data.FechaEstadoControl= pbParams.FechaEstadoControl;
        data.FechaInicioObjetivo= pbParams.FechaInicioObjetivo;
        data.Siniestro= pbParams.Siniestro;
        data.Fechaultimocontrol= pbParams.Fechaultimocontrol;
        data.TipoEncargo = pbParams.TipoEncargo;
        data.Phone = pbParams.Phone;
        data.EstadoAccion = pbParams.EstadoAccion;
        data.FechaAlta = pbParams.FechaAlta;
        data.HoraAccion = pbParams.HoraAccion;
        data.NumEncargo = pbParams.NumEncargo;
        data.Comunicacion = pbParams.Comunicacion;
        
        // Mapping of the Claim from PB
        // ....
        return data;
    }
    
}