@IsTest(SeeAllData=true)
private class H_class_FieldsetControllerTest {

    @IsTest
    static void testMetadata() {
        
        Account testAccount = new Account(Name='Test Account', H_fld_surname__c='Test');
        insert testAccount;
        
        Contact testContact = new Contact(FirstName='Test', LastName='Name', AccountId=testAccount.Id);
        insert testContact;

        Case testCase = new Case(Subject = 'test', Description= 'test',Status='New', ContactId=testContact.Id);
        insert testCase;
        

        H_class_FieldsetController.MetadataResponse resp = H_class_FieldsetController.getObjectMetadata(testAccount.Id);
        System.assertEquals('Account', resp.sObjectName, 'Expected the correct SObject Type to be returned');

        resp = H_class_FieldsetController.getObjectMetadata(testContact.Id);
        System.assertEquals('Contact', resp.sObjectName, 'Expected the correct SObject Type to be returned');
		
        resp = H_class_FieldsetController.getObjectMetadata(testCase.Id);
        System.assertEquals('Case', resp.sObjectName, 'Expected the correct SObject Type to be returned');
        
        resp = H_class_FieldsetController.getFieldSetMetadata(testContact.Id, 'H_fls_datosContacto');
        System.assertEquals('Contact', resp.sObjectName, 'Expected the correct SObject Type to be returned');
        System.assertNotEquals(null, resp.fieldSetLabel, 'Expected to have a field set name');
        System.assertNotEquals(true, resp.fieldsMetadata.isEmpty(), 'Expected to have fields for the field set');

        try {
            resp = H_class_FieldsetController.getFieldSetMetadata(null, null);
            System.assertEquals(true, false, 'Expected the test not to continue');
        } catch (AuraHandledException ex) {
            System.assertEquals(true, true, 'Expected the test to encounter an AuraHandledException');
        }

        try {
            resp = H_class_FieldsetController.getFieldSetMetadata(testAccount.Id, null);
            System.assertEquals(true, false, 'Expected the test not to continue');
        } catch (AuraHandledException ex) {
            System.assertEquals(true, true, 'Expected the test to encounter an AuraHandledException');
        }

        try {
            resp = H_class_FieldsetController.getFieldSetMetadata(testAccount.Id, 'FakeFieldSetNameForTest');
            System.assertEquals(true, false, 'Expected the test not to continue');
        } catch (AuraHandledException ex) {
            System.assertEquals(true, true, 'Expected the test to encounter an AuraHandledException');
        }

    }

}