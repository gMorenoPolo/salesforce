public without sharing class H_class_GetClaimFromSubjectCase {
    @InvocableMethod(label='Get Claim from Subject Case' description='Get Claim from Subject Case' category='Claim')
    public static List<Claim> getClaimFromSubjectCase(List<String> email) {
        List<Claim> claimsRetrieved = null;
        List<String> references = null;
        for(String str : email){
            List<String> splitEmail = str.split('(?U)[\\s.,]+');
            Pattern pt = Pattern.compile('([0-9]{2}[a-zA-Z][0-9]{6})');
            for (String s : splitEmail) {
                if(references == null){
                    references = new List<String>();
                }
                Matcher m = pt.matcher(s);
                if (m.find()) {
                    references.add(s);
                }
            }
        }
        if(references.size() > 0){
            claimsRetrieved = [SELECT Id, Name, PolicyNumberId, AccountId FROM Claim WHERE Name IN :references];
        }
        return claimsRetrieved;
    }

}