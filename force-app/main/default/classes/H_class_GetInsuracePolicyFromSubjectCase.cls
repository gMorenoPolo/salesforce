public without sharing class H_class_GetInsuracePolicyFromSubjectCase {
    @InvocableMethod(label='Get Insurace Policy from Subject Case' description='Get Insurace Policy from Subject Case' category='InsuracePolicy')
    public static List<InsurancePolicy> getInsuracePolicyFromSubjectCase(List<String> email) {
        List<InsurancePolicy> policiesRetrieved = null;
        List<String> references = null;
        for(String str : email){
            List<String> splitEmail = str.split('(?U)[\\s.,]+');
            Pattern pt = Pattern.compile('(\\w{2})(^|\\s{0,5})([A-Z|a-z]\\w{1}\\d{1})(^|\\s{0,7})(\\d{1,7})');
            for (String s : splitEmail) {
                if(references == null){
                    references = new List<String>();
                }
                Matcher m = pt.matcher(s);
                if (m.find()) {
                    references.add(s);
                }
            }
        }
        if(references.size() > 0){
            policiesRetrieved = [SELECT Name, NameInsuredId FROM InsurancePolicy WHERE Name IN :references];
        }
        return policiesRetrieved;
    }
}