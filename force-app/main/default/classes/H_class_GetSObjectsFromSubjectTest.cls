@IsTest
public class H_class_GetSObjectsFromSubjectTest {

    @testSetup static void setup() {
        Account testAccount = new Account(Name='Test Account', H_fld_surname__c='Test');
        insert testAccount;
        // Create common test policy
        List<InsurancePolicy> testPolicies = new List<InsurancePolicy>();

        testPolicies.add(new InsurancePolicy(Name = 'O0H150002729', NameInsuredId=testAccount.Id));

        insert testPolicies;
    }
    
    @isTest
    static void getInsuracePolicyFromSubjectCaseTest(){
        Test.startTest();
        List<InsurancePolicy> policies = H_class_GetInsuracePolicyFromSubjectCase.getInsuracePolicyFromSubjectCase(new List<String>{'Hola que tal O0H150002729'});
        Test.stopTest();
        // Las SOSL de Salesforce devuelve 0 resultados en los test:
        System.assert(policies.size()>0);
    }
    
    @isTest
    static void getClaimFromSubjectCaseTest(){
        Test.startTest();
        List<Claim> claims = H_class_GetClaimFromSubjectCase.getClaimFromSubjectCase(new List<String>{'Hola que tal 06H053912'});
        Test.stopTest();
        // Las SOSL de Salesforce devuelve 0 resultados en los test:
        System.assert(claims.size()==0);
    }
    
    @isTest
    static void getTransactionFromCaseTest(){
        Test.startTest();
        List<InsurancePolicyTransaction> policyTransactions = H_class_GetTransactionFromCase.getTransactionFromCase(new List<String>{'Hola que tal 06H053912'});
        Test.stopTest();
        // Las SOSL de Salesforce devuelve 0 resultados en los test:
        System.assert(policyTransactions==null);
    }
}