public without sharing class H_class_GetTransactionFromCase {
    @InvocableMethod(label='Get Policy Transaction from Case' description='Get Policy Transaction from Case' category='InsurancePolicyTransaction')
    public static List<InsurancePolicyTransaction> getTransactionFromCase(List<String> email) {
        List<InsurancePolicyTransaction> transactionsRetrieved = null;
        List<String> references = null;
        for(String str : email){
            List<String> splitEmail = str.split('(?U)[\\s.,]+');
            Pattern pt = Pattern.compile('([0-9]{10})');
            for (String s : splitEmail) {
                if(references == null){
                    references = new List<String>();
                }
                Matcher m = pt.matcher(s);
                if (m.find()) {
                    references.add(s);
                }
            }
        }
        if(references.size() > 0){
            transactionsRetrieved = [SELECT Id, Name, InsurancePolicyId, H_fld_codCliente__c FROM InsurancePolicyTransaction WHERE Name IN :references];
        }
        return transactionsRetrieved;
    }

}