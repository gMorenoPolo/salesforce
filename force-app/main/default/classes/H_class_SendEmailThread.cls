public without sharing class H_class_SendEmailThread {
    
    @InvocableMethod(label='Send Email Thread' description='Send Email Thread when a Case status is changed to Closed' category='Case')
    public static List <Result> sendEmailThread(List<Case> caseList) {
        
        System.debug('Inicio de método sendEmailThread con case: '+caseList[0]);
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        List<EmailMessage> emList = [
            SELECT id, ContentDocumentIds, Subject,HtmlBody, TextBody, FromName, MessageDate, FromAddress, ToAddress, ReplyToEmailMessageId
            FROM Emailmessage
            WHERE ParentId =:caseList[0].id AND Status NOT IN ('5','2')
            ORDER BY createdDate desc
        ];
        // Iteramos sobre todos los correos para reunir el hilo correos al email que pretendemos enviar:
        String body = '';
        Iterator<EmailMessage> iter = emList.iterator();
        while(iter.hasNext()){
            EmailMessage email = iter.next();
            body +=email.HTMLBody !=null ? email.HTMLBody : email.TextBody;
            if(iter.hasNext()){
                body += '<br></br>--------------- Previous Email ---------------<br></br>From: &nbsp;'+ email.FromAddress +' <br></br>Sent: &nbsp; '+ email.MessageDate + '<br></br>To: &nbsp; '+ email.ToAddress + '<br></br>Subject: &nbsp; '+ email.Subject + '<br></br>';
            }
        }
        List<Id> attachments = null;
        for (EmailMessage emailItem : emList){
            // Se realiza una búsqueda de aquellos adjuntos que se han vinculado con los emails de salida:
            for(ContentDocumentLink linkObj :[SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: emailItem.Id]){
                if(attachments == null){
                    attachments = new List<Id>();
                }
                List<ContentVersion> docList = [SELECT Id from ContentVersion where ContentDocumentId =: linkObj.ContentDocumentId];
                for(ContentVersion doc : docList){
                    attachments.add(doc.Id);
                }
            }
            // Se realiza una búsqueda de aquellos adjuntos que se han vinculado con los emails entrantes:
            for(Attachment attachmentObj :[Select Id From Attachment where ParentId =: emailItem.Id]){
                if(attachments == null){
                    attachments = new List<Id>();
                }
                attachments.add(attachmentObj.Id);
            }
        }
        // Se realiza una búsqueda de aquellos adjuntos que se hayan podido incorporar a parte en el caso:
        for (ContentDocumentLink linkObj: [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: caseList[0].Id]){
            if(attachments == null){
                attachments = new List<Id>();
            }
            List<ContentVersion> docList = [SELECT Id from ContentVersion where ContentDocumentId =: linkObj.ContentDocumentId];
            for(ContentVersion doc : docList){
                attachments.add(doc.Id);
            }
        }
        if(caseList[0].Subject != null){
            mail.setSubject(caseList[0].Subject);
        }else{
            mail.setSubject('Email Thread belongs to Case '+caseList[0].Id +' that has not its subject field populated.');
        }
        mail.setHtmlBody(body);
        if(attachments != null){
            mail.setEntityAttachments(attachments);
        }
        mail.setToAddresses(new List<String>{'repproduqua@helvetia.es'});
        mail.setTreatBodiesAsTemplate(true);
        mail.setBccSender(false);
        mail.setUseSignature(false);
        mail.setSaveAsActivity(false);
        
       	mail.setReplyTo(UserInfo.getUserEmail());
       	mail.setSenderDisplayName(UserInfo.getName()); 
        mails.add(mail);  
        
        List<Messaging.SendEmailResult> results =  Messaging.sendEmail(mails);
        
        Result result = new Result();
        // we can then check for success
        if (results[0].success) {
            result.error = false;
            System.debug('The email sent successfully');
            return new List<Result>{result};
        } else {
            result.error = true;
            System.debug('The email failed to send:  ' +  results[0].errors[0].message);
            return new List<Result>{result};
        }
    }
    
    public class Result {
        @InvocableVariable(label='Records for Output' description='check if was bad' required=true)
        public Boolean error;
    }
}