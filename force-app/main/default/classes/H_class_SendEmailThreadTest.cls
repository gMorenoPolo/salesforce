@IsTest
public class H_class_SendEmailThreadTest {
    
    @testSetup static void setup() {
        // Create common test accounts
        List<Account> testAccts = new List<Account>();
        for(Integer i=0;i<2;i++) {
            testAccts.add(new Account(Name = 'TestAcct'+i, H_fld_surname__c = 'Prueba'));
        }
        insert testAccts;   
        
        // Create common test cases
        List<Case> testCases = new List<Case>();
        for(Integer i=0;i<2;i++) {
            testCases.add(new Case(Subject='test', Description='test',AccountId = testAccts[0].Id, Origin = 'Email', Priority = 'Medium', Status = 'Closed'));
        }
        insert testCases;   
        
        // if EnhancedEmail Perm is enabled, create an EmailMessage object
        EmailMessage emailMessage = new EmailMessage();
        emailMessage.status = '3'; // email was sent
        emailMessage.relatedToId = testCases[0].Id; // related to record e.g. an opportunity
        emailMessage.fromAddress = 'sender@example.com'; // from address
        emailMessage.fromName = 'Dan Perkins'; // from name
        emailMessage.subject = 'This is the Subject!'; // email subject
        emailMessage.htmlBody = '<html><body><b>Hello</b></body></html>'; // email body
        // additional recipients who don’t have a corresponding contact, lead or user id in the Salesforce org (optional)
        emailMessage.toAddress = 'emailnotinsalesforce@toexample.com, anotherone@toexample.com';
        insert emailMessage; // insert
        
        ContentVersion content=new ContentVersion(); 
        content.Title='Header_Picture1'; 
        content.PathOnClient='/' + content.Title + '.jpg'; 
        Blob bodyBlob=Blob.valueOf('Unit Test ContentVersion Body'); 
        content.VersionData=bodyBlob; 
        //content.LinkedEntityId=sub.id;
        content.origin = 'H';
        insert content;
        ContentDocumentLink contentlink=new ContentDocumentLink();
        contentlink.LinkedEntityId=emailMessage.id;
        contentlink.contentdocumentid=[select contentdocumentid from contentversion where id =: content.id].contentdocumentid;
        contentlink.ShareType = 'V';
        contentlink.Visibility = 'AllUsers'; 
        
        
        insert contentlink;
    }
    
    @IsTest
    static void testSendEmailThread() {
        DMLException dmlEx = new DMLException('Test Ex Email');
        List<Case> caseList = [SELECT Id, Subject FROM Case];
        Test.startTest();
        List <H_class_SendEmailThread.Result> resultList = H_class_SendEmailThread.sendEmailThread(caseList);
        Test.stopTest();
        System.assertEquals(false, resultList[0].error);
    }
}