global virtual class H_class_communicationService {

    global class H_class_QueueMKT implements System.Queueable, Database.AllowsCallouts {

        private final String method;
        private final String body;

        global H_class_QueueMKT (String method, String body) {
            this.method = method;
            this.body = body;
        }

        global void execute(System.QueueableContext ctx) {
            HttpRequest req = new HttpRequest();
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('Authorization', 'Bearer {!$Credential.OAuthToken}');
            req.setEndpoint('callout:H_nc_mktCredential/interaction/v1/events');
            req.setMethod(method);
            req.setBody(body);
            System.debug('body:'+req.getBody());
            Http http = new Http();
            HttpResponse res = http.send(req);
            if (res.getStatusCode() != 201) { //retry

                DateTime Dt = system.now().addMinutes(1);
                String day = string.valueOf(Dt.day());
                String month = string.valueOf(Dt.month());
                String hour = string.valueOf(Dt.hour());
                String minute = string.valueOf(Dt.minute());
                String year = string.valueOf(Dt.year());
                String ScheduledTime = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;

                H_class_AutoService job = new H_class_AutoService(this.body);
                System.schedule('H_class_AutoService Job'+Dt, ScheduledTime, job);
            }
            System.debug(JSON.serialize(res.getBody()));

        }

    }

    public static String findContactKey(String accountID){
        Contact basicContact = [SELECT Id FROM Contact WHERE AccountId =:accountID AND H_fld_basicContact__c=true LIMIT 1];
        String contactId = '';
        if(basicContact != null){
            contactId = basicContact.Id;
        }
        return contactId;
    }

    public static String generateBody(String contactKey, String eventId, Object data){
        Map<String,Object> body = new Map<String,Object>();
        body.put('Contactkey',contactKey);
        body.put('EventDefinitionKey',eventId);
        body.put('Data',data);
        system.debug('DATOS:'+JSON.serializePretty(body, true));
        return JSON.serializePretty(body, true);
    }

    public static void sendEvent(String body){
        //Encolamos la llamada asíncrona:
        System.debug('DATOS TO CALLOUT:'+body);
        System.enqueueJob(new H_class_QueueMKT('POST', body));
    }

}