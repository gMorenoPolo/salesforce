@isTest
global class H_class_communicationServiceMock implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest request) {
        // Create a fake response
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{"eventInstanceId": "dfa08f30-29f0-47d7-8168-7d1c6f866ee0"}');
        response.setStatusCode(201);
        return response; 
    }
}