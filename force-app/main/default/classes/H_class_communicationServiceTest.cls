@isTest
public class H_class_communicationServiceTest {
    
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    
    @isTest 
    static void testPostCalloutOK() {
        //Generate data to call mehtod sendEventToMkt:
        H_class_ClaimCommunicationService.Request params = new H_class_ClaimCommunicationService.Request();
        params.EventDefinitionKey = 'APIEvent-aa444c44-4a1d-fb44-bff4-4ee444db7444';
        Id recordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName='H_rt_clientesFisicos'].Id;
        Account acc = new Account(Name='TESTT', H_fld_correoelectronico__c='testt@testt.com', H_fld_surname__c='testt', RecordTypeId=recordTypeId);
        insert acc;
        //String accountId = [SELECT id FROM Account Where Name='testt' LIMIT 1].id;
        params.AccountId = acc.Id;
        params.Name = 'Testt';
        List<H_class_ClaimCommunicationService.Request> paramList = new List<H_class_ClaimCommunicationService.Request>();
        paramList.add(params);
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new H_class_communicationServiceMock()); 
        // This causes a fake response to be sent
        // from the class that implements HttpCalloutMock.
        Test.startTest();
        H_class_ClaimCommunicationService.sendEventToMkt(paramList);
        Test.stopTest();
		
    }
    
    @isTest 
    static void testServiceScheduled() {
        Test.setMock(HttpCalloutMock.class, new H_class_communicationServiceMock()); 
        Test.startTest();

          // Schedule the test job
          String jobId = System.schedule('Test my class',
                            CRON_EXP, 
                            new H_class_AutoService(''));
    
          // Get the information from the CronTrigger API object
          CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
             NextFireTime
             FROM CronTrigger WHERE id = :jobId];
    
          // Verify the expressions are the same
          System.assertEquals(CRON_EXP, 
             ct.CronExpression);
    
          // Verify the job has not run
          System.assertEquals(0, ct.TimesTriggered);
    
          // Verify the next time the job will run
          System.assertEquals('2022-03-15 00:00:00', 
             String.valueOf(ct.NextFireTime));
    
          Test.stopTest();
		
    }

}