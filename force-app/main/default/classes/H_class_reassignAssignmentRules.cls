public class H_class_reassignAssignmentRules {

     /*
     * Pass the ID values to a future method, and perform your ownership changea synchronously.
     * This will allow you to reassign the user correctly after the assignment rule logic has executed.
     * Source: sfdcfox https://salesforce.stackexchange.com/a/40004/15114
     */

    @future
    public static void reassignCaseWithActiveRule(ID myCaseId, Boolean moreThanOneGroup, Integer groupNumber, Boolean isRC) {

        //fetching the desired assignment rules on Case...
        List<AssignmentRule> ARList = [select id from AssignmentRule where SobjectType = 'Case' and Active = true and Name like 'H_car_CP%'];

        if (!ARList.isEmpty()){
            //recreating the DMLOptions setting for "Assign using active assignment rules" checkbox on Case object...
            Database.DMLOptions dmlOpts = new Database.DMLOptions();
            dmlOpts.assignmentRuleHeader.assignmentRuleId= ARList[0].id;
    
            //get current instance of sObject (Case)...
            List<Case> caseList = [SELECT Id,Status,H_fld_moreThanOneGroup__c from Case WHERE Id =: myCaseId];

            if (!caseList.isEmpty()){
                if(groupNumber>0){
                   caseList[0].Status = 'Pendiente Asignar Usuario'; 
                }
                caseList[0].H_fld_moreThanOneGroup__c = moreThanOneGroup;
                caseList[0].H_fld_isRC__c = isRC;
                
                //set DMLOptions on this record so it will be processed suing assignment rules...
                caseList[0].setOptions(dmlOpts);
                //caseList[0].description = 'processed w/ @future on '+ dateTime.now();
        		
                update caseList[0]; 
            }
        }

    }
    
}