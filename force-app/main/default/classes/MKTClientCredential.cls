public class MKTClientCredential extends Auth.AuthProviderPluginClass {

	private String redirectUrl;
	private String key;
    private String secret;
    private String authUrl;
    private String accessTokenUrl;
    private String accountId;
    private String userAPIUrl;
    
	public String getCustomMetadataType() {
		return 'MKT_Setting__mdt';
	}

	public PageReference initiate(Map<String, String> config, String stateToPropagate) {
        authUrl = config.get('Auth_Url__c');
        key =  config.get('Client_id__c');
        redirectUrl = config.get('Redirect_Url__c');
        String url = authUrl + '?response_type=code&client_id='+ key +'&redirect_uri='+ redirectUrl + '&state=' + stateToPropagate;
        return new PageReference(url);
	}

	public Auth.AuthProviderTokenResponse handleCallback(Map<String, String> config, Auth.AuthProviderCallbackState callbackState) {
        key = config.get('Client_id__c');
        secret = config.get('Client_secret__c');
        accessTokenUrl = config.get('Token_endpoint__c');
        accountId = config.get('account_id__c');
        redirectUrl = config.get('Redirect_Url__c');
        
        Map<String,String> queryParams = callbackState.queryParameters;
        String code = queryParams.get('code');
        String sfdcState = queryParams.get('state');
        
        HttpRequest req = new HttpRequest();
        String url = accessTokenUrl;
        req.setEndpoint(url);
        req.setHeader('Content-Type','application/json');
        String body ='{"grant_type": "authorization_code","code": '+'"'+code+'"'+',"client_id": '+'"'+key+'"'+',"client_secret": '+'"'+secret+'"'+',"redirect_uri": '+'"'+redirectUrl+'"'+',"account_id": '+accountId+'}';
        /*String payLoad = 'grant_type=authorization_code' 
            + '&code=' + EncodingUtil.urlEncode(code,'UTF-8') 
            + '&client_id=' + EncodingUtil.urlEncode(key,'UTF-8') 
            + '&client_secret=' + EncodingUtil.urlEncode(secret,'UTF-8') 
            + '&redirect_uri=' + EncodingUtil.urlEncode(redirectUrl,'UTF-8')
        	+ '&account_id=' + EncodingUtil.urlEncode(accountId,'UTF-8');*/
        req.setBody(body);
        system.debug(body);
        req.setMethod('POST');
        
        Http http = new Http();
        HTTPResponse res = http.send(req);
        String responseBody = res.getBody();
        
        Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(responseBody);
        String accessToken,refreshToken;
        if(res.getStatusCode() == 200){
            accessToken = (String)results.get('access_token');
            refreshToken = (String)results.get('refresh_token');
        }
        System.debug('TOKEN:'+accessToken);
        System.debug('REFRESH-TOKEN:'+refreshToken);
        System.debug('RESULT TOKEN TIME:'+results.get('expires_in'));
        System.debug('RESULT TOKEN SCOPE:'+results.get('scope'));
        return new Auth.AuthProviderTokenResponse('MKTCustom', accessToken, refreshToken , sfdcState);
        //don’t hard-code the refresh token value!
    }

	public Auth.UserData getUserInfo(Map<String, String> config, Auth.AuthProviderTokenResponse response) {
        //Here the developer is responsible for constructing an Auth.UserData object
        String token = response.oauthToken;
        HttpRequest req = new HttpRequest();
        System.debug(token);
        userAPIUrl = config.get('API_User_Url__c');
        req.setHeader('Authorization', 'Bearer ' + token);
        req.setEndpoint(userAPIUrl);
        req.setHeader('Content-Type','application/json');
        req.setMethod('GET');
        
        Http http = new Http();
        HTTPResponse res = http.send(req);
        String responseBody = res.getBody();
        Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(responseBody);
        Object user = (Object) results.get('user');
        String userS = JSON.serialize(user);
        Map<String, Object> resultUser = (Map<String, Object>)JSON.deserializeUntyped(userS);
        String id = (String) resultUser.get('sub');
        String flname = (String) resultUser.get('name');
        String uname = (String) resultUser.get('email');
        String locale = (String) resultUser.get('locale');
        Map<String,String> provMap = new Map<String,String>();
        provMap.put('what1', 'noidea1');
        provMap.put('what2', 'noidea2');
        return new Auth.UserData(id, flname.split(' ')[0], flname.split(' ')[1], flname, uname,
                                 'what', flname, locale, 'MKTCustom', null, provMap);
	}
    

	public override Auth.OAuthRefreshResult refresh(Map<String, String> config, String refreshToken) {
        key = config.get('Client_id__c');
        secret = config.get('Client_secret__c');
        accessTokenUrl = config.get('Token_endpoint__c');
        accountId = config.get('account_id__c');
        
		System.debug(LoggingLevel.WARN, 'refresh-config: ' + config);
		System.debug(LoggingLevel.WARN, 'refresh-refreshToken: ' + refreshToken);
        
        HttpRequest req = new HttpRequest();
        String url = accessTokenUrl;
        req.setEndpoint(url);
        req.setHeader('Content-Type','application/json');
        String body ='{"grant_type": "refresh_token","client_id": '+'"'+key+'"'+',"client_secret": '+'"'+secret+'"'+',"refresh_token": '+'"'+refreshToken+'"'+',"account_id": '+accountId+'}';
        req.setBody(body);
        system.debug(body);
        req.setMethod('POST');
        
        Http http = new Http();
        HTTPResponse res = http.send(req);
        String responseBody = res.getBody();
        
        Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(responseBody);
        String accessToken,latterRefreshToken;
        if (res.getStatusCode() != 200) {
            String errorMessage = 'Access Token refresh failed. Error: ' + res.toString();
            return new Auth.OAuthRefreshResult(null,null,errorMessage);
        }
        accessToken = (String)results.get('access_token');
        latterRefreshToken = (String)results.get('refresh_token');
        System.debug('TOKEN:'+accessToken);
        System.debug('REFRESH-TOKEN:'+refreshToken);
        System.debug('RESULT TOKEN TIME:'+results.get('expires_in'));
        System.debug('RESULT TOKEN SCOPE:'+results.get('scope'));
        
		return new Auth.OAuthRefreshResult(accessToken,latterRefreshToken);
	}
}