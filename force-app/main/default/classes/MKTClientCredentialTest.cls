@IsTest
public class MKTClientCredentialTest {
    
    private static final String OAUTH_TOKEN = 'testToken';
    private static final String STATE = 'mocktestState';
    private static final String REFRESH_TOKEN = 'refreshToken';
    private static final String LOGIN_ID = '898732878';
    private static final String USERNAME = 'Jorge Ruiz';
    private static final String FIRST_NAME = 'Jorge';
    private static final String LAST_NAME = 'Ruiz';
    private static final String EMAIL_ADDRESS = 'jorge.ruiz@techedgegroup.com';
    private static final String LOCALE_NAME = 'testLocalName';
    private static final String FULL_NAME = FIRST_NAME + ' ' + LAST_NAME;
    private static final String PROVIDER = 'MKTCustom';
    private static final String REDIRECT_URL = 'https://helvetia2--devhelv.my.salesforce.com/services/authcallback/MKTCustom';
    private static final String KEY = 'testKey';
    private static final String SECRET = 'testSecret';
    private static final String STATE_TO_PROPOGATE  = 'testState';
    private static final String ACCESS_TOKEN_URL = 'https://mccbg961xjjmf7sms5sbwc0v9tj8.auth.marketingcloudapis.com/v2/token';
    private static final String ACCOUNT_ID = '55555555';
    private static final String AUTH_URL = 'https://mccbg961xjjmf7sms5sbwc0v9tj8.auth.marketingcloudapis.com/v2/authorize';
    private static final String API_USER_URL = 'https://mccbg961xjjmf7sms5sbwc0v9tj8.auth.marketingcloudapis.com/v2/userinfo';
    
    // in the real world scenario , the key and value would be read from the (custom fields in) custom metadata type record
    private static Map<String,String> setupAuthProviderConfig () {
        Map<String,String> authProviderConfiguration = new Map<String,String>();
        authProviderConfiguration.put('Client_id__c', KEY);
        authProviderConfiguration.put('Auth_Url__c', AUTH_URL);
        authProviderConfiguration.put('Client_secret__c', SECRET);
        authProviderConfiguration.put('Token_endpoint__c', ACCESS_TOKEN_URL);
        authProviderConfiguration.put('API_User_Url__c',API_USER_URL);
        authProviderConfiguration.put('account_id__c',ACCOUNT_ID);
        authProviderConfiguration.put('Redirect_Url__c',REDIRECT_URL);
        return authProviderConfiguration;
        
    }
    
    static testMethod void testInitiateMethod() {
        String stateToPropogate = 'mocktestState';
        Map<String,String> authProviderConfiguration = setupAuthProviderConfig();
        MKTClientCredential mkt = new MKTClientCredential();
        PageReference expectedUrl =  new PageReference(authProviderConfiguration.get('Auth_Url__c') + '?client_id='+ 
                                                       authProviderConfiguration.get('Client_id__c') +'&redirect_uri='+ 
                                                       authProviderConfiguration.get('Redirect_Url__c') + '&response_type=code'+
                                                       '&state=' + 
                                                       STATE_TO_PROPOGATE);
        
        PageReference actualUrl = mkt.initiate(authProviderConfiguration, STATE_TO_PROPOGATE);
        System.assertEquals(expectedUrl.getUrl(), actualUrl.getUrl());
    }
    
    static testMethod void testHandleCallback() {
        Map<String,String> authProviderConfiguration = setupAuthProviderConfig();
        MKTClientCredential mkt = new MKTClientCredential();
        authProviderConfiguration.get('Redirect_Url_c');
        
        Test.setMock(HttpCalloutMock.class, new MKTMockHttpResponseGenerator());
        
        Map<String,String> queryParams = new Map<String,String>();
        queryParams.put('code','code');
        queryParams.put('state',authProviderConfiguration.get('State_c'));
        Auth.AuthProviderCallbackState cbState = new Auth.AuthProviderCallbackState(null,null,queryParams);
        Auth.AuthProviderTokenResponse actualAuthProvResponse = mkt.handleCallback(authProviderConfiguration, cbState);
        Auth.AuthProviderTokenResponse expectedAuthProvResponse = new Auth.AuthProviderTokenResponse('MKTCustom', OAUTH_TOKEN, REFRESH_TOKEN, null);
        
        System.assertEquals(expectedAuthProvResponse.provider, actualAuthProvResponse.provider);
        System.assertEquals(expectedAuthProvResponse.oauthToken, actualAuthProvResponse.oauthToken);
        System.assertEquals(expectedAuthProvResponse.oauthSecretOrRefreshToken, actualAuthProvResponse.oauthSecretOrRefreshToken);
        System.assertEquals(expectedAuthProvResponse.state, actualAuthProvResponse.state);
        
        
    }
    
    
    static testMethod void testGetUserInfo() {
        Map<String,String> authProviderConfiguration = setupAuthProviderConfig();
        MKTClientCredential mkt = new MKTClientCredential();
        
        Test.setMock(HttpCalloutMock.class, new MKTMockHttpResponseGenerator2());
        
        Auth.AuthProviderTokenResponse response = new Auth.AuthProviderTokenResponse(PROVIDER, OAUTH_TOKEN ,'sampleOauthSecret', STATE);
        Auth.UserData actualUserData = mkt.getUserInfo(authProviderConfiguration, response) ;
        
        Map<String,String> provMap = new Map<String,String>();
        provMap.put('what1', 'noidea1');
        provMap.put('what2', 'noidea2');
        
        Auth.UserData expectedUserData = new Auth.UserData(LOGIN_ID, FIRST_NAME, LAST_NAME, FULL_NAME, EMAIL_ADDRESS,
                                                           'what', FULL_NAME, 'es', PROVIDER, null, provMap);
        
        System.assertNotEquals(expectedUserData,null);
        System.assertEquals(expectedUserData.firstName, actualUserData.firstName);
        System.assertEquals(expectedUserData.lastName, actualUserData.lastName);
        System.assertEquals(expectedUserData.fullName, actualUserData.fullName);
        System.assertEquals(expectedUserData.email, actualUserData.email);
        System.assertEquals(expectedUserData.username, actualUserData.username);
        System.assertEquals(expectedUserData.locale, actualUserData.locale);
        System.assertEquals(expectedUserData.provider, actualUserData.provider);
        System.assertEquals(expectedUserData.siteLoginUrl, actualUserData.siteLoginUrl);
    }
    
    static testMethod void testRefreshToken() {
        Map<String,String> authProviderConfiguration = setupAuthProviderConfig();
        MKTClientCredential mkt = new MKTClientCredential();
        		        
        Test.setMock(HttpCalloutMock.class, new MKTMockHttpResponseGenerator());
        Auth.OAuthRefreshResult actual = mkt.refresh(authProviderConfiguration,REFRESH_TOKEN);
        
        Auth.OAuthRefreshResult expected = new Auth.OAuthRefreshResult(OAUTH_TOKEN,REFRESH_TOKEN);
        System.assertNotEquals(expected,null);
    }
    
    
    // implementing a mock http response generator for MKT 
    public  class MKTMockHttpResponseGenerator implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"access_token":'+'"'+OAUTH_TOKEN+'","refresh_token":'+'"'+REFRESH_TOKEN+'"}');
            res.setStatusCode(200);
            return res;
        }
        
    }
    
    // implementing a mock http response generator for MKT 
    public  class MKTMockHttpResponseGenerator2 implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            Map<String,Map<String,String>> userMap = new Map<String,Map<String,String>>();
            Map<String,String> useratributes = new Map<String,String>();
            useratributes.put('sub', '898732878');
            useratributes.put('name', 'Jorge Ruiz');
            useratributes.put('email', 'jorge.ruiz@techedgegroup.com');
            useratributes.put('locale', 'es');
            userMap.put('user', useratributes);
            res.setBody(JSON.serialize(userMap));
            res.setStatusCode(200);
            return res;
        }
        
    }
}