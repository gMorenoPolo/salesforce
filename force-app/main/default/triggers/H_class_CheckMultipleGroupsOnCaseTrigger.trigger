trigger H_class_CheckMultipleGroupsOnCaseTrigger on Case (before insert, before update, after update) {
    
    if (Trigger.isBefore && !System.isFuture()) {
        if (Trigger.isInsert) {
            //Buscamos el recordtype del centro de producción:
            Id recordTypeCentroProdId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('H_rt_centroProduccion').getRecordTypeId();
            for (Case c : Trigger.new) {
                //comprobamos si es centro de produccion:
                if(c.RecordTypeId == recordTypeCentroProdId) {
                    Integer groupNumber = H_class_CheckAssigmentRulesHandler.isMeetingMultipeCriteria(c);
                    c.H_fld_isRC__c = H_class_CheckAssigmentRulesHandler.getRCdetected();
                    if(groupNumber>1){
                        c.H_fld_moreThanOneGroup__c = true; 
                    }
                    else {
                        c.H_fld_moreThanOneGroup__c = false; 
                    }
                    if(groupNumber==1){
                        c.Status = 'Pendiente asignar usuario'; 
                    }
                }            
            }   
        }
        
        if (Trigger.isUpdate) {
            Boolean moreThan1 = false;
            //Buscamos el recordtype del centro de producción:
            Id recordTypeCentroProdId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('H_rt_centroProduccion').getRecordTypeId();
            for (Case c : Trigger.new) {
                System.debug('Handling Case: '+c.Id);
                //comprobamos si es centro de produccion:
                if(c.RecordTypeId == recordTypeCentroProdId) {
                    List<Group> grp = [SELECT Id, Type, DeveloperName From Group Where Id=:c.OwnerId];
                    if (grp.size() > 0){
                        if ( grp[0].Type=='Queue' && grp[0].DeveloperName == 'H_que_grupoSaco'){
                            Integer groupNumber = H_class_CheckAssigmentRulesHandler.isMeetingMultipeCriteria(c);
                            if(groupNumber>1){
                                moreThan1 = true; 
                            }
                            // Forzamos las reglas de asignación
                            H_class_reassignAssignmentRules.reassignCaseWithActiveRule(c.Id,moreThan1,groupNumber,H_class_CheckAssigmentRulesHandler.getRCdetected());
                        }
                    }
                }
            }
        }
    }
    
    if(Trigger.isAfter && !System.isFuture()){
        if (Trigger.isUpdate) {
            //Buscamos el recordtype del centro de producción:
            Id recordTypeCentroProdId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('H_rt_centroProduccion').getRecordTypeId();
            for (Case c : Trigger.new) {
                System.debug('Handling Case: '+c.id);
                if(c.RecordTypeId == recordTypeCentroProdId) {
                    List<Group> grp = [SELECT Id, Type, DeveloperName From Group Where Id=:c.OwnerId];
                    // El caso esta siendo tratado por un usuario:
                    if (grp.size() == 0){
                        //Lanzamos el flow de email alert Helvetia_EmailAlert_Caso_Aceptado
                        if(c.IsClosed && c.Status=='Aceptada' && c.H_fld_numeroPoliza__c != null){
                            Flow.Interview flow = new Flow.Interview.Helvetia_EmailAlert_Caso_Aceptado(new map<String,Object>  {'Caso' => c.Id});
                            flow.start();
                        }     
                    }
                }
            }
        }
    }
}